namespace :format_intl_translations do
  class String
    def unindent
      indent = self.split("\n").select {|line| !line.strip.empty? }.map {|line| line.index(/[^\s]/) }.compact.min || 0
      self.gsub(/^[[:blank:]]{#{indent}}/, '')
    end
    def unindent!
      self.replace(self.unindent)
    end
  end

  def file_content(file)
    File.read(file)
  end

  def file_first_line(file)
    File.open(file, &:readline)
  end

  def file_locale(file)
    File.basename(file, '.*')
  end

  def has_intl_format?(file)
    !file_first_line(file).include?(file_locale(file)) &&
      !file_first_line(file).include?("---")
  end

  def has_i18n_format?(file)
    file_first_line(file).include?(file_locale(file)) ||
      file_first_line(file).include?("---")
  end

  def replace_file_content(file, new_content)
    File.open(file, "w") { |f| f.puts new_content}
  end

  def format_intl_to_i18n(file)
    new_content = "#{file_locale(file)}:\n#{file_content(file).indent(2)}"
    replace_file_content(file, new_content)
  end

  def format_i18n_to_intl(file)
    after_locale_line = file_first_line(file).include?("---") ? 2 : 1
    new_content = file_content(file)
                    .split("\n")[after_locale_line..-1].join("\n")
                    .unindent
    replace_file_content(file, new_content)
  end

  desc "Detects format of locale files and translates to intl or i18n formats depending on the file configuration"
  task format: :environment do
    Dir.glob("#{Rails.root}/config/locales/**/*.yml").map do |file|
      if has_intl_format?(file)
        format_intl_to_i18n(file)
      else
        format_i18n_to_intl(file)
      end
    end
  end

  desc "Formats i18n translations placed inside config/locales folder to intl rails translations."
  task format_i18n: :environment do
    Dir.glob("#{Rails.root}/config/locales/**/*.yml").map do |file|
      format_i18n_to_intl(file) if has_i18n_format?(file)
    end
  end

  desc "Formats intl translations placed inside config/locales folder to i18n rails translations."
  task format_intl: :environment do
    Dir.glob("#{Rails.root}/config/locales/**/*.yml").map do |file|
      format_intl_to_i18n(file) if has_intl_format?(file)
    end
  end

  task generate_client_translations: :environment do
    Rake::Task['format_intl_translations:format_intl'].invoke
    system("i18n-tasks translate-missing --backend=deepl --from=#{ENV['DEFAULT_LANGUAGE']} #{ENV['TRANSLATABLE_LANGUAGES']}")
    Rake::Task['format_intl_translations:format_i18n'].invoke
  end

  task generate_api_translations: :environment do
    system("i18n-tasks translate-missing --backend=deepl --from=#{ENV['DEFAULT_LANGUAGE']} #{ENV['TRANSLATABLE_LANGUAGES']}")
  end
end

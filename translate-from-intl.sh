#!/bin/sh

echo "1. --- Translating Files From intl to i18n"
# bundle exec rails format_intl_translations:format_intl
# i18n-tasks translate-missing --backend=deepl --from=de en ru
# bundle exec rails format_intl_translations:format_i18n
echo "2. --- Files successfully translated. You can find them under config/locales :)"
# Select Passenger + Ruby 2.5 Official Phusion Distribution
# =========================================================
FROM ruby:2.6.5

# Install netcat for controlling when Database is properly
# configured.
# ========================================================
RUN apt-get update

# Copy application into container and set up permissions
# ======================================================
WORKDIR /home/app/i18n-translator

# Set up bundler
# ==============
# Fixed version because v3.2.0 is broken
RUN gem update --system 3.1.5
RUN gem install bundler

# Improving Docker caching for bundle install
# ===========================================
COPY Gemfile /home/app/i18n-translator
COPY Gemfile.lock /home/app/i18n-translator
RUN bundle install

COPY . /home/app/i18n-translator

ENTRYPOINT bundle exec rails format_intl_translations:generate_client_translations